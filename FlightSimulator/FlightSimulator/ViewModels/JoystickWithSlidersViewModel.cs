﻿using FlightSimulator.Model.Services;
using FlightSimulator.Model.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightSimulator.ViewModels
{
    public class JoystickWithSlidersViewModel : BaseNotify
    {
        public event ChangeValueOfProperty<float> OnValueChanged;
        public JoystickWithSlidersViewModel()
        {
            flightSimulator = FlightSimulatorService.Instence;
            OnValueChanged += flightSimulator.OnValueChange;
            OnValueChanged += ((propertyName, newValue) => NotifyPropertyChanged(propertyName));
        }

        private float aileron=0;
        public float Aileron
        {
            get
            {
                return aileron;
            }
            set
            {
                aileron = value;
                OnValueChanged(SimulatorConstants.Aileron, value);
            }
        }

        private float throttle=0;
        public float Throttle
        {
            get
            {
                return throttle;
            }
            set
            {
                throttle = value;
                OnValueChanged(SimulatorConstants.Throttle, value);
            }
        }

        private float elevator=0;
        public float Elevator
        {
            get
            {
                return elevator;
            }
            set
            {
                elevator = value;
                OnValueChanged(SimulatorConstants.Elevator, value);
            }
        }

        private float rudder=0;
        public float Rudder
        {
            get
            {
                return rudder;
            }
            set
            {
                rudder = value;
                OnValueChanged(SimulatorConstants.Rudder, value);
            }
        }

        public FlightSimulatorService flightSimulator { get; set; }

    }
}
