﻿using FlightSimulator.Model;
using FlightSimulator.Model.Services;
using FlightSimulator.Model.TCPServerHandlers;
using FlightSimulator.TCPCommunication;
using FlightSimulator.Views.Windows;
using System.Net;
using System.Windows.Input;

namespace FlightSimulator.ViewModels
{
    public class FlightSimulatorViewModel : BaseNotify
    {
        #region Commands
        #region SettingCommand
        private ICommand settingCommand;
        public ICommand SettingCommand
        {
            get
            {
                return settingCommand ?? (settingCommand = new CommandHandler(() => OnSettingClick()));
            }
        }

        // showing the Settings window at the moment the Settings button is pressed.
        private void OnSettingClick()
        {
            SettingsWindow settingsWindow = new SettingsWindow();
            settingsWindow.Show();
        }

        #endregion
        #region ConnectCommand
        private ICommand connectCommand;
        public ICommand ConnectCommand
        {
            get
            {
                return connectCommand ?? (connectCommand = new CommandHandler(() => OnConnectClick()));
            }
        }
        //actinating the two channels at the moment the Connect button os pressed.
        private void OnConnectClick()
        {
            if (FlightSimulatorService.Instence.TcpClient == null)
            {
                // Info-Channel - creating the server and stars listening.
                int serverPort = ApplicationSettingsModel.Instance.FlightInfoPort;
                FlightSimulatorService.Instence.TcpServer = new TCPServer(ipAddress: IPAddress.Loopback, port: serverPort, handler: new FlightSimulatorHandler());
                FlightSimulatorService.Instence.TcpServer.openServer();

                // Command-Channel.
                IPAddress address = IPAddress.Parse(ApplicationSettingsModel.Instance.FlightServerIP);
                int commandsPort = ApplicationSettingsModel.Instance.FlightCommandPort;
                FlightSimulatorService.Instence.TcpClient = new TCPClient(address,commandsPort);
            }
        }

        #endregion
        #endregion
    }
}