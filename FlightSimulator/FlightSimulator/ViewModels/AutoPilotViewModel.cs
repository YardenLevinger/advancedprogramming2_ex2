﻿using FlightSimulator.Model;
using FlightSimulator.Model.Services;
using FlightSimulator.TCPCommunication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;

namespace FlightSimulator.ViewModels
{
    public class AutoPilotViewModel : BaseNotify
    {
        private string commandsStr;
        private bool isBackgroundRed = false;
        public bool IsBackgroundRed
        {
            get => isBackgroundRed;
            set
            {
                isBackgroundRed = value;
                NotifyPropertyChanged("IsBackgroundRed");
            }
        }
        public string CommandsStr
        {
            get => commandsStr;
            set
            {
                commandsStr = value;
                if (value.Any())   // if the string is not empty- the background will be painted with red.
                {
                    IsBackgroundRed = true;
                }
                else
                {
                    IsBackgroundRed = false;    // at the moment the string is empty again the background will be white again.
                }
                NotifyPropertyChanged("CommandsStr");
            }
        }

        #region Commands
        #region OKCommand
        private ICommand okCommand;
        public ICommand OKCommand
        {
            get
            {
                return okCommand ?? (okCommand = new CommandHandler(() => OnOKClick()));
            }
        }
        private void OnOKClick()
        {
            // sending the commands through the command channel using the Client's Write function.
            new Thread(() =>
            {
                Console.WriteLine();
                string[] separators = { Environment.NewLine };
                // spliting to the actual commands and putting them inside an array.
                IEnumerable<string> commands = CommandsStr.Split(separators, StringSplitOptions.RemoveEmptyEntries)
                                                .Select(command => command.Trim())
                                                .Where(command => command.Count() > 0);
                // sending each command in the array to the simulator.
                foreach (string command in commands)
                {
                    FlightSimulatorService.Instence.TcpClient.Write(command + "\r\n");
                    // waiting for two seconds before sending the next command.
                    Thread.Sleep(TimeSpan.FromSeconds(2));
                }

                IsBackgroundRed = false;

            }).Start();
        }
        #endregion

        #region ClearCommand
        private ICommand clearCommand;

        public ICommand ClearCommand
        {
            get
            {
                return clearCommand ?? (clearCommand = new CommandHandler(() => OnClearClick()));
            }
        }
        // convert the current string with an empty one.
        private void OnClearClick()
        {
            CommandsStr = "";
        }
        #endregion

        #endregion
    }
}
