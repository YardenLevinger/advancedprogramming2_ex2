﻿using Microsoft.Research.DynamicDataDisplay.DataSources;
using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Windows;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using FlightSimulator.TCPCommunication;
using System.Net;
using System.Xml;
using System.Diagnostics;
using FlightSimulator.Model.TCPServerHandlers;
using FlightSimulator.Model.Services;
using System.ComponentModel;

namespace FlightSimulator.ViewModels
{
    public class FlightBoardViewModel
    {
        private readonly ObservableDataSource<Point> planeLocations = new ObservableDataSource<Point>();
        public ObservableDataSource<Point> PlaneLocations => planeLocations;
        public ObservableCollection<Point> lst;
        private bool isLatReceived = false;
        private bool isLonReceived = false;

        public FlightSimulatorService Service { get; set; }
        
        public FlightBoardViewModel()
        {
            Service = FlightSimulatorService.Instence;
            planeLocations.SetXYMapping(p => p);
            Service.PropertyChanged += Vm_PropertyChanged;
        }

        // drawing the dots on the graph
        private void Vm_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName.Equals("Lat"))
            {
                isLatReceived = true;
            }
            if (e.PropertyName.Equals("Lon"))
            {
                isLonReceived = true;
            }
            if (isLatReceived && isLonReceived)
            {
                if(Service.Lon != 0 && Service.Lat != 0)
                {
                    // creating the new point and adding it to the graph.
                    Point p1 = new Point(Service.Lat, Service.Lon);
                    PlaneLocations.AppendAsync(Application.Current.Dispatcher, p1);
                    isLonReceived = isLatReceived = false;
                }                
            }
        }
    }
}
