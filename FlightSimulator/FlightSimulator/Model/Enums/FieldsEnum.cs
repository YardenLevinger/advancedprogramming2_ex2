﻿namespace FlightSimulator.Model.Enums
{
    public enum fields:byte
    {
        longitude_deg = 0,
        latitude_deg = 1,
        airspeed_indicator_indicated_speed_kt = 2,
        altimeter_indicated_altitude_ft = 3,
        altimeter_pressure_alt_ft = 4,
        attitude_indicator_indicated_pitch_deg = 5,
        attitude_indicator_indicated_roll_deg = 6,
        attitude_indicator_internal_pitch_deg = 7,
        attitude_indicator_internal_roll_deg = 8,
        encoder_indicated_altitude_ft = 9,
        encoder_pressure_alt_ft = 10,
        gps_indicated_altitude_ft = 11,
        gps_indicated_ground_speed_kt = 12,
        gps_indicated_vertical_speed = 13,
        indicated_heading_deg = 14,
        magnetic_compass_indicated_heading_deg = 15,
        slip_skid_ball_indicated_slip_skid = 16,
        turn_indicator_indicated_turn_rate = 17,
        vertical_speed_indicator_indicated_speed_fpm = 18,
        flight_aileron = 19,
        flight_elevator = 20,
        flight_rudder = 21,
        flight_flaps = 22,
        engine_throttle = 23,
        engine_rpm = 24
    }
}
