﻿using FlightSimulator.TCPCommunication;
using FlightSimulator.ViewModels;
using FlightSimulator.Model.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Threading;
using System.Windows;

namespace FlightSimulator.Model.Services
{
    public delegate void ChangeValueOfProperty<T>(string propertyName, T value);
    // the only source that holds the updated information.
    public class FlightSimulatorService : BaseNotify
    {
        private const double IntervalBetweenSendingCommands = 0.1;

        private float lat;
        public float Lat { get { return lat; } set { if (lat != value) { lat = value; NotifyPropertyChanged("Lat"); } } }
        private float lon;
        public float Lon { get { return lon; } set { if (lon != value) { lon = value; NotifyPropertyChanged("Lon"); } } }
        private float aileron;
        public float Aileron { get { return aileron; } set { if (aileron != value) { aileron = value; NotifyPropertyChanged("Aileron"); } } }
        private float elevator;
        public float Elevator { get { return elevator; } set { if (elevator != value) { elevator = value; NotifyPropertyChanged("Elevator"); } } }
        private float rudder;
        public float Rudder { get { return rudder; } set { if (rudder != value) { rudder = value; NotifyPropertyChanged("Rudder"); } } }
        private float throttle;
        public float Throttle { get { return throttle; } set { if (throttle != value) { throttle = value; NotifyPropertyChanged("Throttle"); } } } 
        public TCPServer TcpServer { get; set; }
        public TCPClient TcpClient { get; set; }

        private ConcurrentDictionary<string, float> nodesChanged = new ConcurrentDictionary<string, float>();
        private Timer timerForSendingChanges =null;

        private FlightSimulatorService()
        {
            Application.Current.Exit += OnExit;
        }

        private void OnExit(object sender, System.EventArgs e)
        {
            // calling to the Client's Dispose function in order to releas all allocated resourses.
            TcpClient?.Dispose();
        }

        private static FlightSimulatorService instance = null;
        public static FlightSimulatorService Instence
        {
            get
            {
                if (instance == null)
                    instance = new FlightSimulatorService();
                return instance;
            }
        }

        public void OnValueChange(string propertyName, float newValue)
        {
            nodesChanged[propertyName] = newValue;
            if(timerForSendingChanges==null)
                timerForSendingChanges = new Timer(SendCommand, null, TimeSpan.Zero, TimeSpan.FromSeconds(IntervalBetweenSendingCommands));
        }

        private void SendCommand(object state)
        {
            lock (nodesChanged)
            {
                IEnumerable<string> propertiesChanged = nodesChanged.Keys;
                //converts the commands to set-commands and sends them to the simulator using the Client's Write function.
                foreach (string property in propertiesChanged)
                {
                    string nodePath = SimulatorConstants.FieldToNodePathDictionary[property];
                    TcpClient?.Write("set " + nodePath + " " + nodesChanged[property] + "\r\n");
                }
                nodesChanged.Clear();
            }
        }
    }
}
