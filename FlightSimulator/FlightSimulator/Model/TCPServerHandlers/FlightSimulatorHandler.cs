﻿using FlightSimulator.Model.Enums;
using FlightSimulator.Model.Interface;
using FlightSimulator.Model.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightSimulator.Model.TCPServerHandlers
{
    public class FlightSimulatorHandler : IMessagesHandler
    {
        //initiallizing our servise class with the valuse we were getting from the simulator.
        public void handle(string line)
        {
            IEnumerable<float> values = line.Split(',').Select(str => float.Parse(str.Trim()));

            FlightSimulatorService.Instence.Aileron = values.ElementAt((int)fields.flight_aileron);
            FlightSimulatorService.Instence.Elevator = values.ElementAt((int)fields.flight_elevator);
            FlightSimulatorService.Instence.Rudder = values.ElementAt((int)fields.flight_rudder);
            FlightSimulatorService.Instence.Throttle = values.ElementAt((int)fields.engine_throttle);
            FlightSimulatorService.Instence.Lat = values.ElementAt((int)fields.latitude_deg);
            FlightSimulatorService.Instence.Lon = values.ElementAt((int)fields.longitude_deg);
            
        }
    }
}
