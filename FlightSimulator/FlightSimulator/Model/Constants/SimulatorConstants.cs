﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightSimulator.Model.Constants
{
    public static class SimulatorConstants
    {
        public const string Aileron = "Aileron";
        public const string Rudder = "Rudder";
        public const string Elevator = "Elevator";
        public const string Throttle = "Throttle";
        
        // creating a dictionary that holds the vriable's paths and value.
        public static readonly Dictionary<string, string> FieldToNodePathDictionary = new Dictionary<string, string>()
        {
            { Aileron , "/controls/flight/aileron" },
            { Rudder , "/controls/flight/rudder" },
            { Elevator , "/controls/flight/elevator" },
            { Throttle ,"/controls/engines/current-engine/throttle" }
        };
    }
}
