﻿using FlightSimulator.Model;
using FlightSimulator.Model.Enums;
using FlightSimulator.Model.Interface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FlightSimulator.TCPCommunication
{
    public class TCPServer
    {
        private IMessagesHandler handler;
        private IPAddress ipAddress;
        private int port;

        public TCPServer(IPAddress ipAddress, int port, IMessagesHandler handler)
        {
            this.handler = handler;
            this.ipAddress = ipAddress;
            this.port = port;
        }


        public void openServer()
        {
            // waiting for a connection.
            new Thread(() =>
            {
                IPEndPoint ep = new IPEndPoint(this.ipAddress, this.port);
                TcpListener listener = new TcpListener(ep);
                listener.Start();
                Socket clientSocket = listener.AcceptSocket();

                using (NetworkStream stream = new NetworkStream(clientSocket))
                using (StreamReader reader = new StreamReader(stream))
                {
                    while (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine().Trim();
                        // sending the current line to the handler that will split it ans initialize the Servise class.
                        this.handler.handle(line);
                    }
                }

                clientSocket.Close();
                listener.Stop();
            }).Start();
        }

    }
}