﻿using FlightSimulator.Model;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace FlightSimulator.TCPCommunication
{
    // Client class for the Commands Channel.
    public class TCPClient : IDisposable
    {
        private StreamWriter writer;
        private StreamReader reader;
        private NetworkStream stream;
        private TcpClient client;
        public TCPClient(IPAddress ipAddress, int port)
        {
            IPEndPoint ep = new IPEndPoint(ipAddress, port);
            // connecting to the simulator.
            new Thread(() =>
            {
                while (true)
                {
                    try
                    {
                        client = new TcpClient();
                        client.Connect(ep);
                        break;
                    }
                    catch
                    {
                        Thread.Sleep(100);
                    }
                }
                stream = client.GetStream();
                writer = new StreamWriter(stream);
                reader = new StreamReader(stream);
            }).Start();
        }

        // releasing all allocated resources.
        public void Dispose()
        {
            writer?.Dispose();
            stream?.Dispose();
            client?.Close();
        }

        // sending the commands to the simulator.
        public bool Write(string command)
        {
            try
            {
                Console.WriteLine(command);
                writer?.Write(command);
                writer?.Flush();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
